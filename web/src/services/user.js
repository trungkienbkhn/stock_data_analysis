import { Api } from './api'
import { getUserList, deleteUser } from '../constants/admin.api'
import { userProfile, editUser, updateUser } from '../constants/api'

export default {
  async userProfile () {
    try {
      const res = await Api.requestWithToken({
        url: userProfile.url,
        method: userProfile.method
      })
      if (res.status) {
        return res
      }
      return res
    } catch (e) {
      console.log(e)
    }
  },
  async getUserList () {
    try {
      const res = await Api.requestWithToken({
        url: getUserList.url,
        method: getUserList.method
      })
      if (res.status) {
        return res
      }
      return res
    } catch (e) {
      return []
    }
  },
  async deleteUser (id) {
    try {
      const res = await Api.requestWithToken({
        url: `${deleteUser.url}/${id}`,
        method: deleteUser.method
      })

      if (res.status) {
        return res
      }
      return res
    } catch (e) {
      console.log(e)
    }
  },
  async editUser (id) {
    try {
      const res = await Api.requestWithToken({
        url: `${editUser.url}/${id}`,
        method: editUser.method
      })

      if (res.status) {
        return res
      }
      return res
    } catch (e) {
      console.log(e)
    }
  },
  async updateUser (id, data) {
    try {
      const res = await Api.requestWithToken({
        url: `${updateUser.url}/${id}`,
        method: updateUser.method,
        data: data
      })

      if (res.status) {
        return res
      }
      return res
    } catch (e) {
      console.log(e)
    }
  }
}
