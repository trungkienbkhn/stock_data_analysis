import { Api } from './api'
import { login, verifyToken, resetPassword, register } from '../constants/api'
import { forgotPassword } from '@/constants/api'

export default {
  login (data) {
    return Api.request({
      url: login.url,
      method: login.method,
      data
    }).catch((error) => {
      throw new Error(error)
    })
  },

  logout () {
    localStorage.removeItem('token')
    localStorage.removeItem('isAdmin')
    localStorage.removeItem('user')
  },

  async register (data) {
    try {
      return await Api.requestWithToken({
        url: register.url,
        method: register.method,
        data
      })
    } catch (e) {
      console.log(e)
      return null
    }
  },

  async verifyToken () {
    try {
      return await Api.requestWithToken({
        url: verifyToken.url,
        method: verifyToken.method
      })
    } catch (e) {
      console.log('Invalid token')
      return null
    }
  },

  async resetPass (data) {
    try {
      const dataPass = await Api.request({
        url: `${resetPassword.url}`,
        method: resetPassword.method,
        data: data
      })
      return dataPass
    } catch (e) {
      return e.response.data
    }
  },

  async forgotPassword (data) {
    try {
      const dataPass = await Api.request({
        url: forgotPassword.url,
        method: forgotPassword.method,
        data: data
      })
      return dataPass
    } catch (e) {
      return e.response.data
    }
  }
}
