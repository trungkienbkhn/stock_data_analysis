import { Api } from './api'
import {
  bookmark,
  crawlStockAnalysis,
  deleteNotif,
  deleteStock,
  deleteStockAnalysis,
  editEnterprise,
  editStockAnalysis,
  getAnalysis,
  getBookmarks,
  getIndex,
  getLiveboard,
  getNotifs,
  getStockDetail,
  getStockList,
  queryData,
  updateStockAnalysis
} from '@/constants/api'

export default {
  async getLiveboard (id) {
    try {
      const res = await Api.request({
        url: `${getLiveboard.url}?exchange=${id}`,
        method: getLiveboard.method
      })
      return res
    } catch (e) {
      return []
    }
  },
  async getIndex () {
    try {
      const res = await Api.request({
        url: getIndex.url,
        method: getIndex.method
      })
      return res
    } catch (e) {
      return []
    }
  },
  async getNotifs () {
    try {
      const res = await Api.requestWithToken({
        url: getNotifs.url,
        method: getNotifs.method
      })
      return res
    } catch (e) {
      return []
    }
  },
  async getStockDetail (id) {
    try {
      const res = await Api.request({
        url: `${getStockDetail.url}?id=${id}`,
        method: getStockDetail.method
      })
      return res
    } catch (e) {
      return []
    }
  },
  async editEnterprise (id, data) {
    try {
      const res = await Api.requestWithToken({
        url: `${editEnterprise.url}/${id}`,
        method: editEnterprise.method,
        data: data
      })

      if (res.status) {
        console.log('Edit success')
        return res
      }
      return res
    } catch (e) {
      console.log('Not connected')
      return []
    }
  },
  async getAnalysis () {
    try {
      const res = await Api.requestWithToken({
        url: getAnalysis.url,
        method: getAnalysis.method
      })
      if (res.status) {
        return res
      }
      return res
    } catch (e) {
      return []
    }
  },
  async getRelatedAnalysis (id) {
    try {
      const res = await Api.requestWithToken({
        url: `${getAnalysis.url}?id=${id}`,
        method: getAnalysis.method
      })
      return res
    } catch (e) {
      return []
    }
  },
  async getStockList () {
    try {
      const res = await Api.requestWithToken({
        url: getStockList.url,
        method: getStockList.method
      })
      return res
    } catch (e) {
      return []
    }
  },
  async bookmark (id) {
    try {
      return await Api.requestWithToken({
        url: bookmark.url,
        method: bookmark.method,
        data: {
          'stock_id': id
        }
      })
    } catch (e) {
      return []
    }
  },
  async getBookmarks () {
    try {
      return await Api.requestWithToken({
        url: getBookmarks.url,
        method: getBookmarks.method
      })
    } catch (e) {
      return []
    }
  },
  async deleteStock (id) {
    try {
      const res = await Api.requestWithToken({
        url: `${deleteStock.url}/${id}`,
        method: deleteStock.method
      })
      return res
    } catch (e) {
      console.log(e)
    }
  },
  async deleteStockAnalysis (id) {
    try {
      const res = await Api.requestWithToken({
        url: `${deleteStockAnalysis.url}/${id}`,
        method: deleteStockAnalysis.method
      })

      if (res.status) {
        return res
      }
      return res
    } catch (e) {
      console.log(e)
    }
  },
  async editStockAnalysis (id) {
    try {
      const res = await Api.request({
        url: `${editStockAnalysis.url}/${id}`,
        method: editStockAnalysis.method
      })
      if (res) {
        return res
      }
      return res
    } catch (e) {
      return []
    }
  },
  async updateStockAnalysis (id, data) {
    try {
      const res = await Api.requestWithToken({
        url: `${updateStockAnalysis.url}/${id}`,
        method: updateStockAnalysis.method,
        data: data
      })

      if (res.status) {
        console.log('Update success')
        return res
      }
      return res
    } catch (e) {
      console.log('Not connected')
      return []
    }
  },
  async crawlStockAnalysis () {
    try {
      const res = await Api.requestWithToken({
        url: crawlStockAnalysis.url,
        method: crawlStockAnalysis.method
      })

      if (res.status) {
        console.log('Crawl success')
        return res
      }
      return res
    } catch (e) {
      console.log('Crawl failed')
      return []
    }
  },
  async delete_notif (id) {
    try {
      return await Api.requestWithToken({
        url: deleteNotif.url,
        method: deleteNotif.method,
        data: {
          'id': id
        }
      })
    } catch (e) {
      return []
    }
  },
  async query_data (data) {
    try {
      return await Api.request({
        url: queryData.url,
        method: queryData.method,
        data: data
      })
    } catch (e) {
      console.log(e)
    }
  }
}
