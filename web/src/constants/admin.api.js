import { API_URL } from './api'

export const getCompany = {
  method: 'GET',
  url: `${API_URL}/admin/api/companies`
}
export const getUserList = {
  method: 'GET',
  url: `${API_URL}/users`
}

export const deleteUser = {
  method: 'DELETE',
  url: `${API_URL}/user/delete`
}
