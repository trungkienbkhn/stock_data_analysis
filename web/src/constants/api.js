
export const API_URL = 'http://localhost:8000'

export const login = {
  method: 'POST',
  url: `${API_URL}/login`
}

export const register = {
  method: 'POST',
  url: `${API_URL}/register`
}

export const resetPassword = {
  method: 'POST',
  url: `${API_URL}/user/reset_pwd`
}

export const forgotPassword = {
  method: 'POST',
  url: `${API_URL}/api/forgot-pwd`
}

export const getLiveboard = {
  method: 'GET',
  url: `${API_URL}/stock/getliveboard`
}

export const getIndex = {
  method: 'GET',
  url: `${API_URL}/stock/pricelistindex`
}

export const changePassword = {
  method: 'PUT',
  url: `${API_URL}/api/me/change-password`
}

export const verifyToken = {
  method: 'GET',
  url: `${API_URL}/api/token/verify`
}

export const getNotifs = {
  method: 'GET',
  url: `${API_URL}/stock/notification/list`
}

export const getStockDetail = {
  method: 'GET',
  url: `${API_URL}/stock/detail`
}

export const editEnterprise = {
  method: 'POST',
  url: `${API_URL}/stock/update`
}

export const userProfile = {
  method: 'GET',
  url: `${API_URL}/user/profile`
}

export const getAnalysis = {
  method: 'GET',
  url: `${API_URL}/stock/analysis`
}

export const getStockList = {
  method: 'GET',
  url: `${API_URL}/stock/detail`
}

export const bookmark = {
  method: 'POST',
  url: `${API_URL}/stock/bookmark`
}

export const getBookmarks = {
  method: 'GET',
  url: `${API_URL}/stock/bookmark/list`
}

export const deleteStock = {
  method: 'DELETE',
  url: `${API_URL}/stock/delete`
}

export const deleteStockAnalysis = {
  method: 'DELETE',
  url: `${API_URL}/stock/analysis/delete`
}

export const editStockAnalysis = {
  method: 'GET',
  url: `${API_URL}/stock/analysis/edit`
}

export const updateStockAnalysis = {
  method: 'POST',
  url: `${API_URL}/stock/analysis/update`
}

export const crawlStockAnalysis = {
  method: 'GET',
  url: `${API_URL}/stock/analysis/crawl`
}

export const editUser = {
  method: 'GET',
  url: `${API_URL}/user/edit`
}

export const updateUser = {
  method: 'POST',
  url: `${API_URL}/user/update`
}

export const deleteNotif = {
  method: 'DELETE',
  url: `${API_URL}/stock/notification/delete`
}

export const queryData = {
  method: 'POST',
  url: `${API_URL}/stock/query_data`
}
