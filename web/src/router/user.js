import AnalysisScreen from '@/views/stock/analysis-list'
import EnterPriseList from '@/views/stock/enterprise_list'
import EnterPriseDetail from '@/views/stock/enterprise_detail'
import LiveBoard from '@/views/stock/liveboard'
import Profile from '@/views/user/profile'
import StockChart from '@/components/charts/timeseries'
import LiveBoardTest from '@/views/stock/liveboard_test'

export default [
  {
    path: '/stock/analysis',
    name: 'stock.analysis',
    component: AnalysisScreen
  },
  {
    path: '/enterprise/list',
    name: 'enterprise.list',
    component: EnterPriseList
  },
  {
    path: '/enterprise/detail/:id',
    name: 'enterprise.detail',
    component: EnterPriseDetail
  },
  {
    path: '/stock/liveboard',
    name: 'stock.liveboard',
    component: LiveBoard
  },
  {
    path: '/stock/liveboard/test',
    name: 'liveboard.test',
    component: LiveBoardTest
  },
  {
    path: '/stock/stockchart',
    name: 'stock.stockchart',
    component: StockChart
  },
  {
    path: '/user/profile',
    name: 'user.profile',
    component: Profile
  }
]
