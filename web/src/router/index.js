import Vue from 'vue'
import Router from 'vue-router'
import adminRoutes from './admin'
import userRoutes from './user'
import loginScreen from '../views/user/login'
import notFoundScreen from '../views/404'
import home from '../views/home'
import forgotPasswordScreen from '../views/user/forgot-password'
import resetPasswordScreen from '../views/user/change-password'
import userprofile from '../views/user/profile'
import userRegister from '../views/user/register'

Vue.use(Router)

const routesWithPrefix = (prefix, routes) => {
  return routes.map(route => {
    route.path = `${prefix}${route.path}`
    if (prefix.length) {
      route.name = `${prefix.replace('/', '')}.${route.name}`
    }
    return route
  })
}

export const router = new Router({
  linkActiveClass: 'active',
  mode: 'history',
  routes: [
    {
      path: '/user/login',
      name: 'user.login',
      component: loginScreen
    },
    ...routesWithPrefix('/admin', adminRoutes),
    ...routesWithPrefix('', userRoutes),
    {
      path: '/',
      name: 'home',
      component: home
    },
    {
      path: '/user/forgot-password',
      name: 'user.forgot-password',
      component: forgotPasswordScreen
    },
    {
      path: '/user/reset-password',
      name: 'user.reset-password',
      component: resetPasswordScreen
    },
    {
      path: '/user/register',
      name: 'user.register',
      component: userRegister
    },
    {
      path: '*',
      name: 'page-not-found',
      component: notFoundScreen
    },
    {
      path: '/user/profile',
      name: 'user.profile',
      component: userprofile
    }
  ]
})

router.beforeEach((to, from, next) => {
  // console.log(from.path, ' ->', to.path)
  next()
})
