import loginScreen from '@/views/user/login'
import userListScreen from '@/views/admin/user_list'

export default [
  {
    path: '/dashboard',
    name: 'dashboard',
    component: loginScreen
  },
  {
    path: '/user/list',
    name: 'user.list',
    component: userListScreen
  }
]
