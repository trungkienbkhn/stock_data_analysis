#!/bin/sh
pip3 install -r requirements.txt
cd api
python3 manage.py makemigrations
python3 manage.py migrate
python3 command/psql_tools/import_enterprise_data.py
python3 command/psql_tools/import_analysis_data.py
python3 command/psql_tools/create_notif_trigger.py
cd ../web
npm install
