# Stock data analysis

## Requirements
- Backend
    + Python3
    + Module Django, Scrapy, pandas, pdftotext, psycopg2 
- Frontend
    + VueJS
- Database
    + PostgreSql
    + InfluxDB
## Installation

> Operating system: Ubuntu

###PostgreSQL
#### Install PostgreSQL
- Download PostSQL [link](https://www.postgresql.org/download/)
- Install tutorial [link](https://www.youtube.com/watch?v=VNy2nhho9Pg)
#### Clone the remote repository to local

```bash
git clone https://gitlab.com/trungkienbkhn/stock_data_analysis
```

#### Config Database
- Open pg Admin4 or SQL Shell, enter your password
- If you use **pdAdmin4** Create Database in Object tab
![pd Admin 4 image](https://sp.postgresqltutorial.com/wp-content/uploads/2019/05/pgAdmin-connected-to-PostgreSQL-Database-Server.png)
- If you use **SQL Shell**, type in command
```bash
sudo -i -u postgres
psql
CREATE DATABASE yourdatabase;
```
- Open api/myapp/settings.py*
- Change setting configs
```bash
DATABASES = {
        'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'YOUR_DATABASE',
        'USER': 'YOUR_PSQL_USERNAME',
        'PASSWORD': 'YOUR_PSQL_PASSWORD',
        'HOST': 'localhost',
        'PORT': '5432',
    }
}
```
###InfluxDB
####Install InfluxDB

Add the InfluxData repository
```bash
echo "deb https://repos.influxdata.com/ubuntu focal stable" | sudo tee /etc/apt/sources.list.d/influxdb.list
sudo curl -sL https://repos.influxdata.com/influxdb.key | sudo apt-key add -
```

Install & Start InfluxDB Service
```bash
sudo apt-get update && sudo apt-get install influxdb
sudo service influxdb start
```
####Create database
Get in influxDB shell
```bash
influx
```
Get started
```bash
> CREATE DATABASE stockdb
> SHOW DATABASES
> USE stockdb
```
Import Database
```commandline
cd Documents/Projects/stock_data_analysis/crawl_data/crawl_realtime
python import_influxdb.py 
```

## Setup code
- Before installing the code, install the ubuntu packages below
```bash
sudo apt-get install build-essential libpoppler-cpp-dev pkg-config python-dev
sudo apt install libpq-dev python3-dev
```
- Then run setup.sh to setup requirement packets for this system
```bash
bash setup.py
```

### Run code
- Run api server
```bash
python3 api/manage.py runserver
```
- Run web server
If you use npm
```bash
cd web
npm run server
```
Or if you use yarn:
```bash
cd web
yarn serve
```
