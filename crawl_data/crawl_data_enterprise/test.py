import calendar
import json
import time
import pandas as pd
import plotly.graph_objects as go
import requests
from influxdb import InfluxDBClient
from random_user_agent.user_agent import UserAgent
from random_user_agent.params import SoftwareName, OperatingSystem
from datetime import datetime, timedelta


def get_user_agent():
    software_names = [SoftwareName.CHROME.value]
    operating_systems = [OperatingSystem.WINDOWS.value, OperatingSystem.LINUX.value]
    user_agent_rotator = UserAgent(software_names=software_names, operating_systems=operating_systems, limit=100)
    user_agent = user_agent_rotator.get_random_user_agent()
    return user_agent


def get_price(code):
    url = 'https://finance.vietstock.vn/company/tradinginfo'
    header = {
        "Content-Type": "application/json; charset=utf-8",
        "User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.182 Safari/537.36",
        "Host": "finance.vietstock.vn",
        "Origin": "https://finance.vietstock.vn",
        "content-length": "459"
    }
    body = {"code": code, 's': 0}
    response = requests.post(url, headers=header, data=json.dumps(body))
    return json.loads(response.text)


def get_stock_data(stock):
    date = []
    high = []
    low = []
    open = []
    close = []
    totalVal = []
    totalVol = []
    for i in range(len(stock)):
        time = stock[i]['TradingDate'].replace('/Date(', '').replace(')/', '')
        date.append(time)
        high.append(stock[i]['HighestPrice'])
        low.append(stock[i]['LowestPrice'])
        open.append(stock[i]['OpenPrice'])
        close.append(stock[i]['ClosePrice'])
        totalVal.append(stock[i]['TotalVal'])
        totalVol.append(stock[i]['TotalVol'])

    d = {
        'date': date,
        'high': high,
        'low': low,
        'open': open,
        'close': close,
        'total_val': totalVal,
        'total_vol': totalVol
    }
    return d


def get_data_influx(query):
    time = []
    high = []
    low = []
    open = []
    close = []
    totalVal = []
    totalVol = []
    for i in range(len(query)):
        time.append(query[i]['time'])
        high.append(query[i]['high'])
        low.append(query[i]['low'])
        open.append(query[i]['open'])
        close.append(query[i]['close'])
        totalVal.append(query[i]['total_val'])
        totalVol.append(query[i]['total_vol'])

    d = {
        'time': time,
        'high': high,
        'low': low,
        'open': open,
        'close': close,
        'total_val': totalVal,
        'total_vol': totalVol
    }
    return d


def get_url(url, headers, data=''):
    response = requests.get(url, headers=headers, data=json.dumps(data))
    return json.loads(response.text)


# print(d)

def get_client(host, port, db_name):
    client = InfluxDBClient(host=host, port=port)
    client.switch_database(db_name)
    return client


def insert_data(client, data):
    client.drop_measurement('test_measurement')
    for i in range(len(data)):
        json_data = [
            {
                'measurement': 'test_measurement',
                'tags': {
                    'code': 'vni'
                },
                'fields': {
                    'date': data[i]['date'].replace('/Date(', '').replace(')/', ''),
                    'high': data[i]['high'],
                    'low': data[i]['low'],
                    'open': data[i]['open'],
                    'close': data[i]['close'],
                    'total_vol': data[i]['total_vol'],
                    'total_val': data[i]['total_val']
                }
            }
        ]
        client.write_points(json_data)
        time.sleep(1)


def draw_chart(query):
    d = get_data_influx(query)

    df = pd.DataFrame(data=d).iloc[::1]

    fig = go.Figure(data=[go.Candlestick(x=df['time'],
                                         open=df['open'],
                                         high=df['high'],
                                         low=df['low'],
                                         close=df['close'])])
    fig.update_layout(xaxis_rangeslider_visible=False)
    fig.show()


# print(datetime.now())
# print(timedelta(hours=7))
# timestamp = 1619149873
# local_time = datetime.fromtimestamp(timestamp)
# print(local_time)

def get_realtime(code, from_time, to_time):
    url = 'https://api.vietstock.vn/ta/history'
    headers = {
        "Content-Type": "application/json",
        "User-Agent": get_user_agent(),
        "Host": "api.vietstock.vn",
        "Origin": "https://stockchart.vietstock.vn"
    }
    params = {
        'symbol': code,
        'resolution': 'D',
        'from': from_time,
        'to': to_time
    }
    response = requests.get(url, headers=headers, params=params)
    return json.loads(response.text)


# from_time = calendar.timegm(time.gmtime())
# print(from_time)
# time.sleep(5)
# to_time = calendar.timegm(time.gmtime())
# print(to_time)
data = get_realtime('AAA', '1619164903', '1619164903')
data = json.loads(data)
print(data['t'])
