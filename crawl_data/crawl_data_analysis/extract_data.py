import pdftotext
import pandas as pd
import os
from unidecode import unidecode

target_price = []
current_price = []   

def get_price_BVS(pdf_file):
    with open(pdf_file, "rb") as f:
        pdf = pdftotext.PDF(f)
    text = unidecode("".join(pdf)).lower().split('\n')
    target = ''
    current = ''

    for i in range(0,len(text)):
        if target == '':
            if (text[i].count('gia ky vong') > 0) or (text[i].count('gia muc tieu') > 0):
                if text[i].count('(vnd/cp)') > 0:
                    target = text[i].split('(vnd/cp)')[1].strip().split(' ')[0]
                elif text[i].count('(vnd)') > 0:
                    target = text[i].split('(vnd)')[1].strip().split(' ')[0]

        if current == '':
            if (text[i].count('gia thi truong (') > 0) or (text[i].count('thi gia (') > 0):
                current = text[i].split('(')[1].split(')')[1].strip().split(' ')[0]

        if (target != '') and (current != ''):
            break
    
    target = target.replace('.','').replace(',','').strip()
    current = current.replace('.','').replace(',','').strip()
    return target, current

def get_price_PHS(pdf_file):
    with open(pdf_file, "rb") as f:
        pdf = pdftotext.PDF(f)
    text = unidecode("".join(pdf)).lower().split('\n')
    target = ''
    current = ''
    rate = ''

    for i in range(0,len(text)):
        if rate == '':
            if text[i].count('%') > 0:
                rate = text[i].split('%')[0].replace('[','').split(' ')[-1].lstrip('(')

        if target == '':
            if (text[i].count('gia hop ly') > 0) and (text[i].count('vnd') > 0) and (text[i].count('cp') == 0): 
                target = text[i].lstrip('gia hop ly').strip().split(" ")[0]
            elif (text[i].count('gia muc tieu') > 0) and (unidecode(text[i]).count('vnd') > 0) and (text[i].count('cp') == 0): 
                target = text[i].lstrip("gia muc tieu").strip().split(" ")[0]
            
        if current == '':
            if text[i].count('gia hien tai') > 0:
                current = text[i].lstrip("gia hien tai").strip().split(" ")[0]
            elif text[i].count('gia dong cua') > 0:
                current = text[i].split('gia dong cua')[1].strip().split(" ")[0]

        if (target != '') and (current != ''):
            break

    target = target.replace('.','').replace(',','').strip()
    current = current.replace('.','').replace(',','').strip()
    rate = rate.replace(',','.')
    if (target.isnumeric()) and (current.isnumeric() == False) and (rate != ''):
        current = str(round(float(target)/(100+float(rate))/10, 1)).replace('.','')+'00'
    elif current.isnumeric() == False:
        current = ''
    return target, current

def get_price_MRS(pdf_file):
    with open(pdf_file, "rb") as f:
        pdf = pdftotext.PDF(f)
    text = unidecode("".join(pdf)).lower().split('\n')
    target = ''
    current = ''
    rate = ''

    for i in range(0, len(text)):
        if target == '':
            if (text[i].count('gia muc tieu') > 0) and (text[i].count('vnd)') > 0):
                target = text[i].split('vnd)')[1].strip().split(' ')[0]
            elif (text[i].count('gia muc tieu') > 0):
                target = text[i].split('gia muc tieu')[1].replace(':','').replace('vnd','').strip().split(' ')[0].rstrip(',')
                if target != '':
                    rate = text[i+1].split('%')[0].split(' ')[-1].replace('(','')
                elif (target == '') and (text[i+1].count('vnd') > 0):
                    target = text[i+1].split('vnd')[1].strip().split(' ')[0]
                    rate = text[i+2].split('%')[0].split(' ')[-1].replace('(','')
            elif ((text[i].count('tp') > 0) and (text[i].count('vnd') > 0)) or (text[i].count('tp:') > 0):
                target = text[i].split('tp')[1].replace('vnd','').replace(':','').strip().split(' ')[0]
                rate = text[i+1].split('%')[0].split(' ')[-1].replace('(','')

        if current == '':
            if (text[i].count('gia co phieu') > 0) and (text[i].count('vnd)') > 0):
                current = text[i].split('vnd)')[1].strip().split(' ')[0]
            elif (text[i].count('thi gia') > 0) and (text[i].count('vnd') > 0):
                current = text[i].split('vnd)')[1].strip().split(' ')[0]
            elif text[i].count('gia hien tai') > 0:
                if text[i].count('vnd') > 0:
                    current = text[i].split('vnd)')[1].strip().split(' ')[0]
                else:
                    current = text[i].split('gia hien tai')[1].replace('dong','').replace('(','').replace(')','').strip().split(' ')[0]
            elif (text[i].count('gia ngay') > 0) and (text[i].count('vnd') > 0): 
                current = text[i].split('vnd)')[1].strip().split(' ')[0]

        if (target != '') and (current != ''):
            break
    
    target = target.replace('.','').replace(',','').strip()
    current = current.replace('.','').replace(',','').strip()
    rate = rate.replace(',','.')
    if (target.isnumeric()) and (current.isnumeric() == False) and (rate != ''):
        current = str(round(float(target)/(100+float(rate))/10, 1)).replace('.','')+'00'
    elif current.isnumeric() == False:
        current = ''
    return target, current

def get_price_FPT(pdf_file):
    with open(pdf_file, "rb") as f:
        pdf = pdftotext.PDF(f)
    text = unidecode("".join(pdf)).lower().split('\n')
    target = ''
    current = ''

    for i in range(0,len(text)):
        if target == '':
            if text[i].count('gia muc tieu (') > 0:
                target = text[i].split(')')[1].strip().split(' ')[0]
            elif (text[i].count('gia')) and (text[i].count('co phieu')):
                target = text[i].split('co phieu')[1].replace(':','').strip().split(' ')[0]
            elif text[i].count('gia tri noi tai'):
                target = text[i].split('noi tai')[1].replace(':','').strip().split(' ')[0]
            elif text[i].count('gia muc tieu') > 0:
                target = text[i].split('gia muc tieu')[1].replace(':','').strip().split(' ')[0]

        if current == '':
            if text[i].count('gia hien tai ('):
                current = text[i].split(')')[1].replace(':','').strip().split(' ')[0]
            elif text[i].count('gia thi truong (') > 0:
                current = text[i].split(')')[1].strip().split(' ')[0]
            elif text[i].count('gia thi truong:') > 0:
                current = text[i].split(':')[1].strip().split(' ')[0]
            elif text[i].count('gia hien tai') > 0:
                current = text[i].split('gia hien tai')[1].replace(':','').strip().split(' ')[0]

        if (target != '') and (current != ''):
            break
    
    target = target.replace('.','').replace(',','').strip().replace('d','')
    current = current.replace('.','').replace(',','').strip().replace('d','')
    return target, current

def get_price_KIS(pdf_file):
    with open(pdf_file, "rb") as f:
        pdf = pdftotext.PDF(f)
    text = unidecode("".join(pdf)).lower().split('\n')
    target = ''
    current = ''

    for i in range(0,len(text)):
        if target == '':
            if (text[i].count('gia muc tieu')) and (text[i].count('(vnd)')): 
                target = text[i].split(')')[1].replace(':','').strip().split(' ')[0]
            elif text[i].count('gia muc tieu'):
                target = text[i].split('gia muc tieu')[1].replace(':','').strip().split(' ')[0]                 

        if current == '':
            if (text[i].count('gia thi truong')) and (text[i].count('(vnd)')): 
                current = text[i].split(')')[1].replace(':','').strip().split(' ')[0]
            elif text[i].count('gia cp ('):
                current = text[i].split(')')[1].replace(':','').strip().split(' ')[0]
            elif (text[i].count('gia niem yet')) and (text[i].count('(vnd)')): 
                current = text[i].split(')')[1].replace(':','').strip().split(' ')[0]
            elif text[i].count('gia thi truong'):
                current = text[i].split('gia thi truong')[1].replace(':','').strip().split(' ')[0]   

        if (target != '') and (current != ''):
            break
    
    target = target.replace('.','').replace(',','').strip()
    if len(target) == 3:
        target += '00'
    current = current.replace('.','').replace(',','').strip()
    if len(current) == 3:
        current += '00'
    return target, current

def get_price_ACBS(pdf_file):
    with open(pdf_file, "rb") as f:
        pdf = pdftotext.PDF(f)
    text = unidecode("".join(pdf)).lower().split('\n')
    target = ''
    current = ''

    for i in range(0,len(text)):
        if target == '':
            if (text[i].count('gia muc tieu')) and (text[i].count('(vnd)')): 
                target = text[i].split(')')[1].replace(':','').strip().split(' ')[0]               

        if current == '':
            if (text[i].count('gia hien tai')) and (text[i].count('(vnd)')): 
                current = text[i].split(')')[1].replace(':','').strip().split(' ')[0]

        if (target != '') and (current != ''):
            break
    
    target = target.replace('.','').replace(',','').strip()
    current = current.replace('.','').replace(',','').strip()
    return target, current

def get_price_KBSV(pdf_file):
    with open(pdf_file, "rb") as f:
        pdf = pdftotext.PDF(f)
    text = unidecode("".join(pdf)).lower().split('\n')
    target = ''
    current = ''

    for i in range(0,len(text)):
        if target == '':
            if (text[i].count('gia muc tieu')) and (text[i].count('vnd')):
                t = text[i].split('vnd')[0].strip().split(' ')[-1]
                if t.count(',') or t.count('.'):
                    target = t
                else:
                    target = text[i].split('vnd')[1].strip().split(' ')[0]
            elif (text[i].count('gia muc tieu')) and (text[i].count(')')):
                target = text[i].split(')')[1].strip().split(' ')[0]

        if current == '':
            if (text[i].count('gia hien tai') or text[i].count('gia co phieu')) and (text[i].count(')')): 
                current = text[i].split(')')[1].replace(':','').replace('vnd','').strip().split(' ')[0]

        if (target != '') and (current != ''):
            break
    
    target = target.replace('.','').replace(',','').strip()
    current = current.replace('.','').replace(',','').strip()
    return target, current

def get_price_MBS(pdf_file):
    with open(pdf_file, "rb") as f:
        pdf = pdftotext.PDF(f)
    text = unidecode("".join(pdf)).lower().split('\n')
    target = ''
    current = ''
    for i in range(0,len(text)):
        if target == '':
            if (text[i].count('gia muc tieu')) and (text[i].count('vnd')):
                t = text[i].split('vnd')[0].replace('(','').strip().split(' ')[-1]
                if t.count(',') or t.count('.'):
                    target = t
                else:
                    target = text[i].split('vnd')[1].replace(')','').strip().split(' ')[0]
            elif text[i].count('gia muc tieu'):
                t = text[i].split('gia muc tieu')[1].strip().split(' ')[0]
                if t.count(',') or t.count('.'):
                    target = t
                else:
                    target = ''        

        if current == '':
            if (text[i].count('gia hien tai')):
                if text[i].count('vnd'):
                    c = text[i].split('vnd')[0].replace('(','').strip().split(' ')[-1]
                    if c.count(',') or c.count('.'):
                        current = c
                    else:
                        current = text[i].split('vnd')[1].replace(')','').strip().split(' ')[0]
                else:
                    current = text[i].split('gia hien tai')[1].strip().split(' ')[0]
            elif (text[i].count('gia thi truong')) and (text[i].count('vnd')):
                current = text[i].split('vnd')[1].replace(')','').strip().split(' ')[0]
            elif (text[i].count('thi gia')) and (text[i].count(')')):
                current = text[i].split(')')[1].strip().split(' ')[0]
            elif text[i].count('current price'):
                current = text[i].split('current price')[1].strip().split(' ')[0]
            elif (text[i].count('thi gia')) and (text[i].count('vnd')):
                c = text[i].split('vnd')[1].strip().split(' ')[0]
                if c.count(',') or c.count('.'):
                    current = c
                else:
                    current = text[i].split('vnd')[0].strip().split(' ')[-1]

        if (target != '') and (current != ''):
            break
    
    target = target.replace('.','').replace(',','').strip()
    current = current.replace('.','').replace(',','').strip()
    return target, current

data_preprocess = pd.read_csv(os.path.join(os.getcwd(),'data/data_preprocess.csv'))
drop_index = []
for i in range(0, len(data_preprocess)):
    source = data_preprocess.iloc[i]['sources']
    stock_code = data_preprocess.iloc[i]['stock_code']
    pdf = source.replace(' ','_')+'_'+stock_code+'.pdf'
    path = 'data/pdf/'+pdf
    if source == 'FPT Securities':
        [target,current] = get_price_FPT(path)
    elif source == 'Bao Viet Securities':
        [target,current] = get_price_BVS(path)
    elif source == 'KIS':
        [target,current] = get_price_KIS(path)
    elif source == 'ACBS':
        [target,current] = get_price_ACBS(path)
    elif source == 'Phu Hung Securities':
        [target,current] = get_price_PHS(path)
    elif source == 'KBSV':
        [target,current] = get_price_KBSV(path)
    elif source == 'MB Securities':
        [target,current] = get_price_MBS(path)
    elif source == 'Mirae Asset':
        [target,current] = get_price_MRS(path)
    else:
        drop_index.append(i)
        continue

    if target.isnumeric() and current.isnumeric():
        target_price.append(target)
        current_price.append(current)
    else:
        drop_index.append(i)

data_extract = data_preprocess.drop(index=drop_index)
titles = data_extract['titles'].values
recmd = []
rate = []
for i in range(0, len(data_extract)):
    if titles[i].count('-'):
        t = titles[i].split('-')[1].strip().lstrip('Khuyến nghị').strip()
        if t.count(' ') > 3:
            recmd.append('N/A')
        else:
            recmd.append(t)
    else:
        recmd.append('N/A')

    rate.append(str(round((float(target_price[i])*100/float(current_price[i]) - 100), 1))+'%')
    
data_extract['target_price'] = target_price
data_extract['current_price'] = current_price
data_extract['rate'] = rate
data_extract['recommendation'] = recmd
data_extract.to_csv('data_extract.csv', index=False)
