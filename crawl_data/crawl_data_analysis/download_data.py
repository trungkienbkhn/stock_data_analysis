import requests
import pandas as pd
import os

data_preprocess = pd.read_csv(os.path.join(os.getcwd(),'data/data_preprocess.csv'))
urls = data_preprocess['links_download'].values
sources = data_preprocess['sources'].values
code = data_preprocess['stock_code'].values 
for i in range(0,len(urls)):
    source = sources[i].replace(" ","_")
    r = requests.get(urls[i], allow_redirects=True)
    file_name = source+'_'+code[i]+'.pdf'
    file_path = os.path.join(os.getcwd(),('data/pdf/'+file_name))
    open(file_path, 'wb').write(r.content)
