import requests
import json
import os
from scrapy.selector import Selector
import pandas as pd 
from unidecode import unidecode
from datetime import date

today = str(date.today())
companies = []
types = []
sectors = []
drop_index = []
stocks_code = []
type = ''
sector = ''
com_name = ''
stock_code = ''
crawled_time = []

def get_info_report(url):
    response = requests.request("GET", url)
    body = response.text
    com_name = Selector(text=body).xpath('//a[@id="ctl00_webPartManager_wp999337475_wp789463336_lnkSymbol"]/text()').get()
    type = Selector(text=body).xpath('//a[@id="ctl00_webPartManager_wp999337475_wp789463336_lnkCategory"]/text()').get()
    sector = Selector(text=body).xpath('//a[@id="ctl00_webPartManager_wp999337475_wp789463336_lnkSector"]/text()').get()
    return com_name, type, sector

data_raw = pd.read_csv(os.path.join(os.getcwd(),'data/test_data_raw.csv'))
urls = data_raw['links_report'].values
titles = data_raw['titles'].values
for i in range(0, len(data_raw)):
    title = unidecode(titles[i]).lower()
    if title.count('bao cao cap nhat dinh gia') > 0:
        stock_code = title.split('bao cao cap nhat dinh gia')[1].strip().split(' ')[0]
    elif title.count('bao cao tham doanh nghiep') > 0:
        stock_code = title.split('bao cao tham doanh nghiep')[1].strip().split(' ')[0]
    elif title.count('bao cao cap nhat co phieu') > 0:
        stock_code = title.split('bao cao cap nhat co phieu')[1].strip().split(' ')[0]
    elif title.count('bao cao cap nhat tin tuc') > 0:
        stock_code = title.split('bao cao cap nhat tin tuc')[1].strip().split(' ')[0]
    elif title.count('bao cao cap nhat') > 0:
        stock_code = title.split('bao cao cap nhat')[1].strip().split(' ')[0]
    elif title.count('bao cao ngan') > 0:
        stock_code = title.split('bao cao ngan')[1].strip().split(' ')[0]
    elif title.count('bao cao nhanh') > 0:
        stock_code = title.split('bao cao nhanh')[1].strip().split(' ')[0]
    elif title.count('bao cao dinh gia co phieu') > 0:
        stock_code = title.split('bao cao dinh gia co phieu')[1].strip().split(' ')[0]
    elif title.count('bao cao dinh gia') > 0:
        stock_code = title.split('bao cao dinh gia')[1].strip().split(' ')[0]
    elif title.count('bao cao lan dau') > 0:
        stock_code = title.split('bao cao lan dau')[1].strip().split(' ')[0]
    elif titles[i][3] == ':':
        stock_code = title.split(':')[0]
    elif title.count('bao cao phan tich co phieu') > 0:
        stock_code = title.split('bao cao phan tich co phieu')[1].strip().split(' ')[0]
    else:
        stock_code = '' 

    if len(stock_code) == 3:
        info = get_info_report(urls[i])
        if info[0] is None:
            drop_index.append(i)
        else:
            companies.append(info[0])
            types.append(info[1])
            sectors.append(info[2])
            stocks_code.append(stock_code)
    else:
        drop_index.append(i)

data_raw = data_raw.drop(index = drop_index)
data_raw['stock_code'] = stocks_code
data_raw['company'] = companies
data_raw['types'] = types
data_raw['sector'] = sectors
for i in range(0, len(data_raw)):
    crawled_time.append(today)
data_raw['crawled_time'] = crawled_time
data_raw = data_raw.drop("Unnamed: 0",axis=1)
data_raw.to_csv(os.path.join(os.getcwd(),'data/test_data_preprocess.csv'), index = False)
