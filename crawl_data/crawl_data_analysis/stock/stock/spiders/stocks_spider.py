import scrapy
import json
from scrapy.selector import Selector
import pandas

class StocksSpider(scrapy.Spider):
    global reports,links,dates,sources,reports_name
    reports = []
    dates = []
    links = []
    sources = []
    reports_name = []
    name = "stocks"

    def start_requests(self):
        url = "https://www.stockbiz.vn/AnalysisReports.aspx?Type=1"
        for i in range(1,51):
            payload={
                'Cart_ctl00_webPartManager_wp372089597_wp604056795_callbackReports_Callback_Param': str(i)
            }
            yield scrapy.FormRequest(url, callback=self.parse, method='POST', formdata=payload)   

    def parse(self, response):
        global reports,links,dates,sources,reports_name
        body = response.body
        for i in range(1,21):
            if i < 10 :
                params = '//a[@id="ctl00_webPartManager_wp372089597_wp604056795_rptReports_ctl'+'0'+str(i)+'_lnkTitle"]/@href'
                params_name = '//a[@id="ctl00_webPartManager_wp372089597_wp604056795_rptReports_ctl'+'0'+str(i)+'_lnkTitle"]/text()'
                src_params = '//a[@id="ctl00_webPartManager_wp372089597_wp604056795_rptReports_ctl'+'0'+str(i)+'_lnkSource"]/text()'
            else:
                params = '//a[@id="ctl00_webPartManager_wp372089597_wp604056795_rptReports_ctl'+str(i)+'_lnkTitle"]/@href'
                params_name = '//a[@id="ctl00_webPartManager_wp372089597_wp604056795_rptReports_ctl'+str(i)+'_lnkTitle"]/text()'
                src_params = '//a[@id="ctl00_webPartManager_wp372089597_wp604056795_rptReports_ctl'+str(i)+'_lnkSource"]/text()'
            report = Selector(text=body).xpath(params).get()
            source = Selector(text=body).xpath(src_params).get()
            report_name = Selector(text=body).xpath(params_name).get()
            reports.append(report)
            sources.append(source)
            id = report.split("/")[4]
            link = 'https://www.stockbiz.vn/Handlers/DownloadReport.ashx?ReportID='+str(id)
            links.append(link)
            reports_name.append(report_name)
        date = Selector(text=body).xpath('//td[@valign="top"]/text()').getall()
        k = 3
        for i in range(1,21):
            dates.append(date[k].strip())
            k+=11

        df = pandas.DataFrame()
        df['titles'] = reports_name
        df['links_report'] = reports
        df['links_download'] = links
        df['sources'] = sources
        df['published_dates'] = dates
        df.to_csv(os.path.join(os.getcwd().rstrip('stock'),'data/data_raw.csv'))
