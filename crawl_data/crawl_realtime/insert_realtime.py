from influxdb import InfluxDBClient
import random


def get_client(host, port, db_name):
    client = InfluxDBClient(host=host, port=port)
    client.switch_database(db_name)
    return client


def insert_index(data):
    json_data = [
        {
            'measurement': 'index',
            'tags': {
                'code': data['name']
            },
            'fields': {
                'datetime': data['datetime'],
                'index': data['index'],
                'change': data['change'],
                'percent': data['percent'],
                'volume': data['volume'],
                'value': data['value']
            }
        }
    ]
    db.write_points(json_data)


def insert_stock(data):
    json_data = [
        {
            'measurement': 'stock_' + str(data['code']),
            'tags': {
                'code': data['code']
            },
            'fields': {
                'datetime': data['datetime'],
                'open': data['open'],
                'close': data['close'],
                'highest': data['highest'],
                'lowest': data['lowest']
            }
        }
    ]
    db.write_points(json_data)


db = get_client('localhost', '8086', 'stockdb')

indexData = {
    'datetime': '',
    'index': '',
    'change': '',
    'percent': '',
    'volume': '',
    'value': ''
}

stockData = {
    'code': '',
    'datetime': '',
    'open': '',
    'close': '',
    'highest': '',
    'lowest': ''
}

dataType = ''
if dataType == 'index':
    insert_index(indexData)
elif dataType == 'stock':
    insert_stock(stockData)
