from datetime import datetime, timezone, timedelta

from influxdb import InfluxDBClient


def query_data(query_str, code, db):
    client = InfluxDBClient(host='localhost', port='8086')
    client.switch_database(db)
    query = client.query(query_str, epoch='s')
    if code != 'null':
        return list(query.get_points(tags={'code': code}))
    else:
        return list(query.get_points())


def convert_index(query_result):
    time = []
    code = []
    date = []
    index = []
    change = []
    percent = []
    value = []
    volume = []
    for index_code in query_result:
        if float(index_code['index'].replace(',', '')) > 0:
            time.append(datetime.fromtimestamp(index_code['time']).astimezone(timezone(timedelta(hours=7))).strftime("%Y-%m-%d %H:%M:%S"))
            code.append(index_code['code'])
            date.append(index_code['date'])
            index.append(index_code['index'])
            change.append(index_code['change'])
            percent.append(index_code['percent'])
            value.append(index_code['value'])
            volume.append(index_code['volume'])

    json_array = {
        'time': time,
        'code': code,
        'date': date,
        'index': index,
        'change': change,
        'percent': percent,
        'value': value,
        'volume': volume
    }
    return json_array


# Example:
# query_result = query_data("SELECT * FROM index WHERE time >= now()-14h ORDER BY ASC LIMIT 100")


def response_api(query_str, code='null', type='array', db='stockdb'):
    query_result = query_data(query_str, code, db)
    if type == 'array':
        return query_result
    elif type == 'index':
        return convert_index(query_result)
    else:
        return []


# Result
res = response_api(
    "SELECT * FROM index WHERE time >= '2021-05-26T00:00:00.000000000Z' AND time <= '2021-05-27T15:12:00Z' LIMIT 100",
    'VNINDEX', 'index')

print(res)
