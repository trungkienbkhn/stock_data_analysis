import schedule
from datetime import datetime
import time


def clock():
    while True:
        now = datetime.now()
        endtime = now.replace(second=50, microsecond=0)
        print("It's " + now.strftime("%H:%M:%S"))
        if now >= endtime:
            break
        time.sleep(1)


schedule.every().minute.at(":00").do(clock)

while True:
    schedule.run_pending()
    time.sleep(1)
