import requests
import random
import time
from datetime import datetime
import json
import math

from influxdb import InfluxDBClient
from random_user_agent.user_agent import UserAgent
from random_user_agent.params import SoftwareName, OperatingSystem


def get_user_agent():
    software_names = [SoftwareName.CHROME.value]
    operating_systems = [OperatingSystem.WINDOWS.value, OperatingSystem.LINUX.value]
    user_agent_rotator = UserAgent(software_names=software_names, operating_systems=operating_systems, limit=100)
    user_agent = user_agent_rotator.get_random_user_agent()
    return user_agent


def get_code():
    code_array = []
    res = requests.get('http://localhost:8000/stock/getliveboard')
    res = json.loads(res.text)
    for index in res:
        for code in res[index]:
            code_array.append(code['a'])
    return code_array


def get_price(code, type):
    url = 'https://finance.vietstock.vn/company/tradinginfo'
    headers = {
        "Content-Type": "application/json; charset=utf-8",
        "User-Agent": get_user_agent(),
        "Host": "finance.vietstock.vn",
        "Origin": "https://finance.vietstock.vn",
    }
    body = {"code": code, 's': type}
    response = requests.post(url, headers=headers, data=json.dumps(body))
    return json.loads(response.text)


def connect_db():
    client = InfluxDBClient(host='localhost', port='8086')
    client.switch_database('stockdb')
    return client


def make_random(code, refRate):
    price = get_price(code, 0)
    ref = price['PriorClosePrice']
    floor = math.floor(ref * (1 - refRate / 100))
    ceiling = math.ceil(ref * (1 + refRate / 100))
    lowest = random.randint(floor, ceiling)
    highest = random.randint(lowest, ceiling)
    open = random.randint(lowest, highest)
    close = random.randint(open, highest)
    time = datetime.now()

    json_data = [
        {
            'measurement': 'stock_price',
            'tags': {
                'code': code,
                'date': time.strftime("%d/%m/%Y")
            },
            'fields': {
                'datetime': time.strftime("%H:%M:%S"),
                'ref': ref,
                'floor': floor,
                'ceiling': ceiling,
                'open': open,
                'close': close,
                'highest': highest,
                'lowest': lowest
            }
        }
    ]
    return json_data


if __name__ == '__main__':
    db = connect_db()
    code_array = get_code()
    count = 0
    while True:
        if count == 1:
            break
        count += 1
        rand_len = random.randint(0, 50)
        print('ran_len = ' + str(rand_len))
        rand_code = random.choices(code_array, k=rand_len)
        for code in rand_code:
            data = make_random(code, 10)
            db.write_points(data)

        time.sleep(5)
