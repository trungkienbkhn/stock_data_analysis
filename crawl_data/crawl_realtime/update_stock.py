import json
import time
from datetime import datetime
import requests
from influxdb import InfluxDBClient
from random_user_agent.user_agent import UserAgent
from random_user_agent.params import SoftwareName, OperatingSystem
import schedule


def get_user_agent():
    software_names = [SoftwareName.CHROME.value]
    operating_systems = [OperatingSystem.WINDOWS.value, OperatingSystem.LINUX.value]
    user_agent_rotator = UserAgent(software_names=software_names, operating_systems=operating_systems, limit=100)
    user_agent = user_agent_rotator.get_random_user_agent()
    return user_agent


def get_price(code):
    url = 'https://finance.vietstock.vn/company/tradinginfo'
    header = {
        "Content-Type": "application/json; charset=utf-8",
        "User-Agent": get_user_agent(),
        "Host": "finance.vietstock.vn",
        "Origin": "https://finance.vietstock.vn",
        "content-length": "459"
    }
    body = {"code": code, 's': 0}
    response = requests.post(url, headers=header, data=json.dumps(body))
    return json.loads(response.text)


def get_index():
    url = 'https://banggia.cafef.vn/stockhandler.ashx?index=true'
    response = requests.get(url)
    return json.loads(response.text)


def get_url(url, headers, data=''):
    response = requests.get(url, headers=headers, data=json.dumps(data))
    return json.loads(response.text)


def get_client(host, port, db_name):
    client = InfluxDBClient(host=host, port=port)
    client.switch_database(db_name)
    return client


def insert_index(data):
    current_time = datetime.now()
    json_data = [
        {
            'measurement': 'index',
            'tags': {
                'code': data['name'],
                'date': current_time.strftime("%Y-%m-%d")
            },
            'fields': {
                'datetime': current_time.strftime("%H:%M:%S"),
                'index': data['index'],
                'change': data['change'],
                'percent': data['percent'],
                'volume': data['volume'],
                'value': data['value']
            }
        }
    ]
    return json_data


db = get_client('localhost', '8086', 'stockdb')


def update_stock():
    while True:
        index_list = get_index()
        now = datetime.now()
        endtime = now.replace(hour=15, minute=0, second=0, microsecond=0)
        if now >= endtime:
            break
        for index in index_list:
            data = insert_index(index)
            print(data)
            db.write_points(data)
        time.sleep(5)


schedule.every().day.at("09:00").do(update_stock)

while True:
    schedule.run_pending()
    time.sleep(1)
