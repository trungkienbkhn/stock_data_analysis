import asyncio
import websockets
import random
import time
from datetime import datetime
import csv
import json
import pandas as pd
import requests
from influxdb import InfluxDBClient
from random_user_agent.user_agent import UserAgent
from random_user_agent.params import SoftwareName, OperatingSystem


def connect_db():
    client = InfluxDBClient(host='localhost', port='8086')
    client.switch_database('stockdb')
    return client


def get_user_agent():
    software_names = [SoftwareName.CHROME.value]
    operating_systems = [OperatingSystem.WINDOWS.value, OperatingSystem.LINUX.value]
    user_agent_rotator = UserAgent(software_names=software_names, operating_systems=operating_systems, limit=100)
    user_agent = user_agent_rotator.get_random_user_agent()
    return user_agent


def get_index():
    url = 'https://banggia.cafef.vn/stockhandler.ashx?index=true'
    reponse = requests.get(url)
    return json.loads(reponse.text)


def get_stock():
    url = 'https://banggia.cafef.vn/stockhandler.ashx?center=undefined'
    response = requests.get(url)
    return json.loads(response.text)


def get_url(url, data=''):
    headers = {
        "Content-Type": "application/json; charset=utf-8",
        "User-Agent": get_user_agent(),
        "Host": "finance.vietstock.vn",
        "Origin": "https://finance.vietstock.vn",
        "content-length": "459"
    }
    response = requests.get(url, headers=headers, data=json.dumps(data))
    return json.loads(response.text)


def price():
    number = random.randint(0, 100)
    return str(number)


def read_csv_json():
    # json_array = []
    time = []
    code = []
    date = []
    index = []
    change= []
    percent = []
    value = []
    volume = []
    with open('./index_2405.csv', encoding='utf-8') as csvf:
        csvReader = csv.DictReader(csvf)
        for row in csvReader:
            time.append(row['time'])
            code.append(row['code'])
            date.append(row['date'])
            index.append(row['index'])
            change.append(row['change'])
            percent.append(row['percent'])
            value.append(row['value'])
            volume.append(row['volume'])

    json_array = {
        'time': time,
        'code': code,
        'date': date,
        'index': index,
        'change': change,
        'percent': percent,
        'value': value,
        'volumn': volume
    }
    return json_array


def read_csv_dataframe():
    df = pd.read_csv('./index_2405.csv')
    return json.loads(df.to_string())


async def server(websocket, path):
    message = json.dumps(read_csv_json())
    await websocket.send(message)

json_data = read_csv_json()
time = int(json_data['time'][0]).astype(np.int64)
print(time)
print(datetime.fromtimestamp(time).strftime("%H:%M:%S"))
