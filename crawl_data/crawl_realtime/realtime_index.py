import json
import time
from datetime import datetime
import requests
from influxdb import InfluxDBClient
from random_user_agent.user_agent import UserAgent
from random_user_agent.params import SoftwareName, OperatingSystem
import calendar


def get_user_agent():
    software_names = [SoftwareName.CHROME.value]
    operating_systems = [OperatingSystem.WINDOWS.value, OperatingSystem.LINUX.value]
    user_agent_rotator = UserAgent(software_names=software_names, operating_systems=operating_systems, limit=100)
    user_agent = user_agent_rotator.get_random_user_agent()
    return user_agent


def get_index():
    url = 'https://banggia.cafef.vn/stockhandler.ashx?index=true'
    reponse = requests.get(url)
    return json.loads(reponse.text)


def get_client(host, port, db_name):
    client = InfluxDBClient(host=host, port=port)
    client.switch_database(db_name)
    return client


def insert_data(client, data):
    json_data = [
        {
            'measurement': 'index',
            'tags': {
                'code': data['name']
            },
            'fields': {
                'date': datetime.now().strftime("%d/%m/%Y %H:%M:%S"),
                'index': data['index'],
                'change': data['change'],
                'percent': data['percent'],
                'volume': data['volume'],
                'value': data['value']
            }
        }
    ]
    client.write_points(json_data)
    print(json_data)


def update_data(period):
    index_list = get_index()
    for index in index_list:
        insert_data(db, index)
    time.sleep(period)


db = get_client('localhost', '8086', 'stockdb')
print(db)
count = 0

while True:
    update_data(10)
    count += 1
    if count == 10:
        break

results = db.query('SELECT * FROM index')

query = list(results.get_points())
print(query)
