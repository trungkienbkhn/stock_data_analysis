from django.db import models
from django.contrib.auth.models import User

class Stock(models.Model):
    id = models.AutoField(primary_key=True)
    code = models.CharField(max_length=20)
    name = models.CharField(max_length=200)
    description = models.TextField(null=True)
    capital = models.FloatField(null=True)
    traded_volume = models.BigIntegerField(null=True)
    listed_amount = models.BigIntegerField(null=True)
    circulating_amount = models.BigIntegerField(null=True)

class Analysis(models.Model):
    id = models.AutoField(primary_key=True)
    stock = models.ForeignKey(Stock,on_delete=models.CASCADE,null=True)
    target_price = models.BigIntegerField()
    current_price = models.BigIntegerField()
    rate = models.CharField(max_length=10)
    recommendation = models.CharField(max_length=50, null=True)
    title = models.CharField(max_length=200, null=True)
    sector = models.CharField(max_length=200, null=True)
    source = models.CharField(max_length=200, null=True)
    link_download = models.CharField(max_length=200, null=True)
    link_report = models.CharField(max_length=200, null=True)
    published_date = models.DateField()
    crawled_time = models.DateField()

class Bookmark(models.Model):
    id = models.AutoField(primary_key=True)
    user = models.ForeignKey(User,on_delete=models.CASCADE)
    stock = models.ForeignKey(Stock,on_delete=models.CASCADE)

class Notification(models.Model):
    id = models.AutoField(primary_key=True)
    bookmark = models.ForeignKey(Bookmark,on_delete=models.CASCADE)
    analysis = models.ForeignKey(Analysis,on_delete=models.CASCADE)
    created_time = models.DateTimeField()
