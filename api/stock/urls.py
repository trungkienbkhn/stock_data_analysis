from django.urls import path
from django.conf.urls import url
from . import views

urlpatterns = [
    path('liveboard', views.liveboard, name="liveboard"),
    url(r'^getliveboard$', views.get_liveboard, name="get_liveboard"),
    path('pricelistindex', views.price_list_index, name="price_list_index"),
    path('analysis',views.analysis, name="analysis"),
    path('analysis/edit/<int:id>',views.analysis_edit, name="analysis_edit"),
    path('analysis/update/<int:id>',views.analysis_update, name="analysis_update"),
    path('analysis/delete/<int:id>',views.analysis_delete, name="analysis_delete"),
    path('analysis/crawl',views.crawl_data_analysis, name="analysis_crawl"),
    path('detail',views.detail, name="detail"),
    path('edit/<int:id>',views.stock_edit, name="stock_edit"),
    path('update/<int:id>',views.stock_update, name="stock_update"),
    path('delete/<int:id>',views.stock_delete, name="stock_delete"),
    path('bookmark',views.bookmark, name="bookmark"),
    path('bookmark/list',views.bookmark_list, name="bookmark_list"),
    path('notification/list',views.notification_list, name="notification_list"),
    path('notification/delete',views.notification_delete, name="notification_delete"),
    path('draw_chart',views.price_chart, name="draw_chart"),
    path('insert_data/index',views.insert_index_realtime_data, name="insert_index_data"),
    path('insert_data/stock',views.insert_stock_realtime_data, name="insert_stock_data"),
    path('query_data',views.query_realtime_data, name="query_realtime_data")
]
