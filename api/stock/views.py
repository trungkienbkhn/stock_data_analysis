from django.http import HttpResponse
from stock.models import Stock, Analysis, Bookmark, Notification
from django.contrib.auth.models import User
from django.views.decorators.csrf import csrf_exempt
from command.update_analysis_data import update_analysis_data
from command.draw_chart import draw_chart, get_url
from command.insert_realtime import get_client, insert_index, insert_stock, response_api
from datetime import datetime
import json
import pytz
import requests
from jose import jwt

vietnam = pytz.timezone('Asia/Ho_Chi_Minh')

def liveboard(request):
    if request.method == "GET":
        stock_code = request.GET.get('stock_code', None)
        url1 = "https://banggia.cafef.vn/stockhandler.ashx?center=undefined"
        response1 = requests.request("GET", url1)
        stock_data = response1.json()

        if stock_code is not None:
            for stock in stock_data:
                if stock['a'].lower() == stock_code.lower():
                    response = stock
                    break
        else:
            url2 = "https://banggia.cafef.vn/stockhandler.ashx?index=true"
            response2 = requests.request("GET", url2)
            stock_exchange = response2.json()

            response = {
                'stock_data': stock_data,
                'stock_exchange': stock_exchange
            }
        return HttpResponse(json.dumps(response), content_type='application/json')

def get_liveboard(request):
    if request.method == "GET":
        exchange = request.GET.get('exchange', None)
        url1 = "https://banggia.cafef.vn/stockhandler.ashx?center=1"
        url2 = "https://banggia.cafef.vn/stockhandler.ashx?center=2"
        url3 = "https://banggia.cafef.vn/stockhandler.ashx?center=9"
        if exchange == '1':
            response = requests.request("GET", url1).json()
        elif exchange == '2':
            response = requests.request("GET", url2).json()
        elif exchange == '3':
            response = requests.request("GET", url3).json()
        else:
            response = {
                "HOSE": requests.request("GET", url1).json(),
                "HNX": requests.request("GET", url2).json(),
                "UPCOM": requests.request("GET", url3).json()
            }
        return HttpResponse(json.dumps(response), content_type='application/json')

def price_list_index(request):
    if request.method == "GET":
        url = "https://banggia.cafef.vn/stockhandler.ashx?index=true"
        response = requests.request("GET", url).json()
        return HttpResponse(json.dumps(response), content_type='application/json')

def analysis(request):
    if request.method == "GET":
        stock_id = request.GET.get('id', None)
        user_info = check_authenticate(request)
        url1 = "https://banggia.cafef.vn/stockhandler.ashx?center=1"
        url2 = "https://banggia.cafef.vn/stockhandler.ashx?center=2"
        url3 = "https://banggia.cafef.vn/stockhandler.ashx?center=9"
        liveboard_data = requests.request("GET", url1).json() + requests.request("GET", url2).json() \
                         + requests.request("GET", url3).json()
        if stock_id is not None:
            stocks = Analysis.objects.filter(stock_id=stock_id).order_by('-published_date')
            if len(stocks) == 0:
                response = {}
            else:
                response = []
                for stock in stocks:
                    stock_code = str(Stock.objects.get(id=stock_id).code)
                    data = {
                        'id': stock.id,
                        'stock_id': stock_id,
                        'stock_code': stock_code,
                        'target_price': stock.target_price,
                        'current_price_report': stock.current_price,
                        'current_price': get_current_price(liveboard_data, stock_code),
                        'rate': float(stock.rate.rstrip('%')),
                        'recommendation': str(stock.recommendation),
                        'company': Stock.objects.get(id=stock_id).name,
                        'sector': 'N/A' if stock.sector is None else stock.sector,
                        'source': stock.source,
                        'title': stock.title,
                        'link_download': stock.link_download,
                        'link_report': stock.link_report,
                        'published_date': str(stock.published_date),
                        'crawled_time': str(stock.crawled_time)
                    }
                    if (user_info != {}):
                        data = add_user_bookmark(data, user_info, stock_id)
                    response.append(data)
        else:
            response = []
            stocks = Analysis.objects.all().order_by('-published_date')
            exist_stock = []
            for stock in stocks:
                if stock.stock_id in exist_stock:
                    continue
                exist_stock.append(stock.stock_id)
                stock_code = str(Stock.objects.get(id=stock.stock_id).code)
                data = {
                    'id': stock.id,
                    'stock_id': stock.stock_id,
                    'stock_code': stock_code,
                    'target_price': stock.target_price,
                    'current_price_report': stock.current_price,
                    'current_price': get_current_price(liveboard_data, stock_code),
                    'rate': float(stock.rate.rstrip('%')),
                    'recommendation': str(stock.recommendation),
                    'company': Stock.objects.get(id=stock.stock_id).name,
                    'sector': 'N/A' if stock.sector is None else stock.sector,
                    'source': stock.source,
                    'title': stock.title,
                    'link_download': stock.link_download,
                    'link_report': stock.link_report,
                    'published_date': str(stock.published_date),
                    'crawled_time': str(stock.crawled_time)
                }
                if (user_info != {}):
                    data = add_user_bookmark(data, user_info, stock.stock_id)
                response.append(data)  
        return HttpResponse(json.dumps(response), content_type='application/json')

@csrf_exempt
def analysis_edit(request, id):
    if request.method == "GET":
        token = request.headers.get('Authorization')
        try:
            data = get_data_from_token(token)
            if data.get('is_admin'):
                try:
                    stock = Analysis.objects.get(id=id)
                    response = {
                        'id': stock.id,
                        'stock_id': stock.stock_id,
                        'stock_code': Stock.objects.get(id=stock.stock_id).code,
                        'company_name': Stock.objects.get(id=stock.stock_id).name,
                        'target_price': stock.target_price,
                        'current_price': stock.current_price,
                        'rate': stock.rate,
                        'recommendation': stock.recommendation,
                        'sector': stock.sector,
                        'source': stock.source,
                        'title': stock.title,
                        'link_download': stock.link_download,
                        'link_report': stock.link_report,
                        'published_date': str(stock.published_date),
                        'crawled_time': str(stock.crawled_time)
                    }
                except:
                    response = {
                        'status': False,
                        'error': 'Not found analysis report'
                    }
            else:
                response = {
                    'status': False,
                    'error': 'Permission error'
                }
        except:
            response = {
                'status': False,
                'error': 'Authentication failed'
            }
        return HttpResponse(json.dumps(response), content_type='application/json')

@csrf_exempt
def analysis_update(request, id):
    if request.method == "POST":
        token = request.headers.get('Authorization')
        try:
            data = get_data_from_token(token)
            if data.get('is_admin'):
                stock = Analysis.objects.filter(id=id)
                if len(stock) > 0:
                    new_data = json.loads(request.body)
                    try:
                        stock.update(target_price=new_data['target_price'], current_price=new_data['current_price'], rate=new_data['rate'],
                                     recommendation=new_data['recommendation'], published_date=new_data['published_date'])
                        response = {
                            'status': True
                        }
                    except:
                        response = {
                            'status': False,
                            'error': "Failed to update analysis report"
                        }
                else:
                    response = {
                            'status': False,
                            'error': "Not found analysis report"
                        }
            else:
                response = {
                    'status': False,
                    'error': 'Permission error'
                }
        except:
            response = {
                'status': False,
                'error': 'Authentication failed'
            }
        return HttpResponse(json.dumps(response), content_type='application/json')

@csrf_exempt
def analysis_delete(request, id):
    if request.method == "DELETE":
        token = request.headers.get('Authorization')
        try:
            data = get_data_from_token(token)
            if data.get('is_admin'):
                try:
                    stock = Analysis.objects.get(id=id)
                    stock.delete()
                    response = {
                        'status': True
                    }
                except:
                    response = {
                        'status': False,
                        'error': 'Failed to delete analysis report'
                    }
            else:
                response = {
                    'status': False,
                    'error': 'Permission error'
                }
        except:
            response = {
                'status': False,
                'error': 'Authentication failed'
            }
        return HttpResponse(json.dumps(response), content_type='application/json')

def detail(request):
    if request.method == "GET":
        stock_id = request.GET.get('id', None)
        user_info = check_authenticate(request)
        if stock_id is not None:
            try:
                stock = Stock.objects.get(id=stock_id)
                response = {
                    'id': stock.id,
                    'stock_code': stock.code,
                    'name': stock.name,
                    'description': stock.description,
                    'capital': stock.capital,
                    'traded_volume': stock.traded_volume,
                    'listed_amount': stock.listed_amount,
                    'circulating_amount': stock.circulating_amount
                }
                if (user_info != {}):
                    response = add_user_bookmark(response, user_info, stock_id)
            except:
                response = {
                    'status': False,
                    'error': 'Not found stock'
                }
        else:
            response = []
            stocks = Stock.objects.all().order_by('id')
            for stock in stocks:
                data = {
                    'id': stock.id,
                    'stock_code': stock.code,
                    'name': stock.name,
                    'description': stock.description,
                    'capital': stock.capital,
                    'traded_volume': stock.traded_volume,
                    'listed_amount': stock.listed_amount,
                    'circulating_amount': stock.circulating_amount
                }
                if (user_info != {}):
                    data = add_user_bookmark(data, user_info, stock.id)
                response.append(data)
        return HttpResponse(json.dumps(response), content_type='application/json')

@csrf_exempt
def stock_edit(request, id):
    if request.method == "GET":
        token = request.headers.get('Authorization')
        try:
            data = get_data_from_token(token)
            if data.get('is_admin'):
                try:
                    stock = Stock.objects.get(id=id)
                    response = {
                        'id': stock.id,
                        'stock_code': stock.code,
                        'name': stock.name,
                        'description': stock.description,
                        'capital': stock.capital,
                        'traded_volume': stock.traded_volume,
                        'listed_amount': stock.listed_amount,
                        'circulating_amount': stock.circulating_amount
                    }
                except:
                    response = {
                        'status': False,
                        'error': 'Not found stock'
                    }
            else:
                response = {
                    'status': False,
                    'error': 'Permission error'
                }
        except:
            response = {
                'status': False
            }
        return HttpResponse(json.dumps(response), content_type='application/json')

@csrf_exempt
def stock_update(request, id):
    if request.method == "POST":
        token = request.headers.get('Authorization')
        try:
            data = get_data_from_token(token)
            if data.get('is_admin'):
                stock = Stock.objects.filter(id=id)
                if len(stock) > 0:
                    new_data = json.loads(request.body)
                    try:
                        stock.update(code=new_data['code'], name=new_data['name'], description=new_data['description'],
                                    capital=new_data['capital'], traded_volume=new_data['traded_volume'],
                                    listed_amount=new_data['listed_amount'], circulating_amount=new_data['circulating_amount'])
                        response = {
                            'status': True
                        }
                    except:
                        response = {
                            'status': False,
                            'error': 'Update stock failed'
                        }
                else:
                    response = {
                        'status': False,
                        'error': 'Not found stock'
                    }
            else:
                response = {
                    'status': False,
                    'error': 'Permission error'
                }
        except:
            response = {
                'status': False,
                'error': 'Authentication error'
            }
        return HttpResponse(json.dumps(response), content_type='application/json')

@csrf_exempt
def stock_delete(request, id):
    if request.method == "DELETE":
        token = request.headers.get('Authorization')
        try:
            data = get_data_from_token(token)
            if data.get('is_admin'):
                try:
                    stock = Stock.objects.get(id=id)
                    stock.delete()
                    response = {
                        'status': True
                    }
                except:
                    response = {
                        'status': False,
                        'error': 'Not found stock'
                    }
            else:
                response = {
                    'status': False,
                    'error': 'Permission error'
                }
        except:
            response = {
                'status': False,
                'error': 'Authentication failed'
            }
        return HttpResponse(json.dumps(response), content_type='application/json')

@csrf_exempt
def bookmark(request):
    if request.method == "POST":
        token = request.headers.get('Authorization')
        try:
            data = get_data_from_token(token)
            current_user = User.objects.get(id = data.get('id'))
            stock_id = json.loads(str(request.body, 'utf-8'))['stock_id']
            ids = list(Bookmark.objects.filter(user_id = current_user.id).values_list('stock_id', flat=True))
            if int(stock_id) in ids:
                Bookmark.objects.filter(user_id = current_user.id, stock_id = stock_id).delete()
                response = {
                    'status': True,
                    'action': 'un bookmark'
                } 
            else:
                Bookmark.objects.create(user_id = current_user.id, stock_id = stock_id)
                response = {
                    'status': True,
                    'action': 'bookmark'
                }  
        except:
            response = {
                'status': False,
                'error': 'Authentication failed'
            }  
        return HttpResponse(json.dumps(response), content_type='application/json')

def bookmark_list(request):
    if request.method == "GET":
        token = request.headers.get('Authorization')
        try:
            data = get_data_from_token(token)
            current_user = User.objects.get(id = data.get('id'))
            bookmarks = Bookmark.objects.filter(user_id = current_user.id)
            if len(bookmarks) > 0:
                response = []
                for bookmark in bookmarks:
                    data = {
                        'id': bookmark.id,
                        'user_id': current_user.id,
                        'stock_id': bookmark.stock_id,
                        'stock_code': Stock.objects.get(id = bookmark.stock_id).code
                    }
                    response.append(data)
            else:
                response = []
        except:
            response = {
                'status': False,
                'error': 'Authentication failed'
            }
        return HttpResponse(json.dumps(response), content_type='application/json')

def notification_list(request):
    if request.method == "GET":
        token = request.headers.get('Authorization')
        try:
            data = get_data_from_token(token)
            current_user = User.objects.get(id = data.get('id'))
            ids = list(Bookmark.objects.filter(user_id = current_user.id).values_list('id', flat=True))
            notifs = Notification.objects.filter(bookmark_id__in=ids)
            if len(notifs) > 0:
                response = []
                for notif in notifs:
                    stock_id = Bookmark.objects.get(id=notif.bookmark_id).stock_id
                    data = {
                        'id': notif.id,
                        'user_id': current_user.id,
                        'code': Stock.objects.get(id=stock_id).code,
                        'stock_id': stock_id,
                        'bookmark_id': notif.bookmark_id,
                        'analysis_id': notif.analysis_id,
                        'target_price': Analysis.objects.get(id=notif.analysis_id).target_price,
                        'recommendation': Analysis.objects.get(id=notif.analysis_id).recommendation,
                        'created_time': calculate_timedelta(notif.created_time)
                    }
                    response.append(data)
            else:
                response = []
        except:
            response = {
                'status': False
            }  
        return HttpResponse(json.dumps(response), content_type='application/json')

@csrf_exempt
def notification_delete(request):
    if request.method == "DELETE":
        token = request.headers.get('Authorization')
        try:
            data = get_data_from_token(token)
            current_user = User.objects.get(id = data.get('id'))
            notif_id = json.loads(str(request.body, 'utf-8'))['id']
            bookmark_ids = list(Bookmark.objects.filter(user_id = current_user.id).values_list('id', flat=True))
            notif_ids = list(Notification.objects.filter(bookmark_id__in=bookmark_ids).values_list('id', flat=True))
            if int(notif_id) in notif_ids:  
                notif = Notification.objects.get(id=notif_id).delete()
                response = {
                    'status': True
                }
            else:
                response = {
                    'status': False
                } 
            
        except:
            response = {
                'status': False,
                'error': 'Authentication failed'
            }  
        return HttpResponse(json.dumps(response), content_type='application/json')

@csrf_exempt
def crawl_data_analysis(request):
    if request.method == "GET":
        token = request.headers.get('Authorization')
        try:
            data = get_data_from_token(token)
            if data.get('is_admin'):
                try:
                    msg = update_analysis_data()
                    response = {
                        'status': True,
                        'message': msg
                    }
                except Error as e:
                    response = {
                        'status': False,
                        'error': e
                    }
            else:
                response = {
                    'status': False,
                    'error': 'Permission error'
                }
        except:
            response = {
                'status': False,
                'error': 'Authentication failed'
            }
        return HttpResponse(json.dumps(response), content_type='application/json')

def price_chart(request):
    if request.method == "GET":
        try:
            url = 'https://finance.vietstock.vn/data/KQGDThongKeGiaStockPaging?page=1&pageSize=20&catID=1&stockID=-19&fromDate=2021-02-11&toDate=2021-03-26'
            headers = {
                "Content-Type": "application/json; charset=utf-8",
                "User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.182 Safari/537.36",
                "Host": "finance.vietstock.vn",
                "Origin": "https://finance.vietstock.vn"
            }
            stock = get_url(url, headers)
            stock = stock[1]
            draw_chart(stock)
            response = {
                'status': True
            }
        except:
            response = {
                'status': False
            }
        return HttpResponse(json.dumps(response), content_type='application/json')

@csrf_exempt
def insert_index_realtime_data(request):
    if request.method == "POST":
        request_data = json.loads(str(request.body, 'utf-8'))
        try:
            db = get_client('localhost', '8086', 'stockdb')
            insert_index(db, request_data)
            response = {
                'status': True
            }
        except:
            response = {
                'status': False,
                'error': 'Unable to insert data'
            }
        return HttpResponse(json.dumps(response), content_type='application/json')

@csrf_exempt
def insert_stock_realtime_data(request):
    if request.method == "POST":
        request_data = json.loads(str(request.body, 'utf-8'))
        try:
            db = get_client('localhost', '8086', 'stockdb')
            insert_stock(db, request_data)
            response = {
                'status': True
            }
        except:
            response = {
                'status': False,
                'error': 'Unable to insert data'
            }
        return HttpResponse(json.dumps(response), content_type='application/json')

@csrf_exempt
def query_realtime_data(request):
    if request.method == "POST":
        request_data = json.loads(str(request.body, 'utf-8'))
        try:
            query_str = request_data.get('query_str')
            code = request_data.get('code') if request_data.get('code') is not None else 'null'
            type = request_data.get('type') if request_data.get('type') is not None else 'array'
            db = request_data.get('db') if request_data.get('db') is not None else 'stockdb'
            response = response_api(query_str, code, type, db)
        except:
            response = {
                'status': False,
                'error': 'Unable to query data'
            }
        return HttpResponse(json.dumps(response), content_type='application/json')

def get_data_from_token(token):
    data = jwt.decode(token.replace('Bearer ',''), 'stock_analysis', algorithms=['HS256'])
    return data

def check_authenticate(request):
    token = request.headers.get('Authorization')
    try:
        data = get_data_from_token(token)
        return data
    except:
        return {}

def add_user_bookmark(data, user_info, stock_id):
    bookmark = Bookmark.objects.filter(user_id=user_info.get('id'), stock_id=stock_id)
    if len(bookmark) > 0:
        data['bookmark'] = True
    else:
        data['bookmark'] = False
    return data

def get_current_price(stock_data, stock_code):
    for stock in stock_data:
        if stock['a'].lower() == stock_code.lower():
            return int(float(stock['b'])*1000)
    return None

def calculate_timedelta(time):
    td = datetime.now() - time.replace(tzinfo=None)
    if td.days == 0:
        td_hour = td.seconds // 3600
        if td_hour == 0:
            return str(td.seconds // 60) + ' minutes ago'
        else:
            return str(td_hour) + ' hours ago'
    else:
        return str(td.days) + ' days ago'
