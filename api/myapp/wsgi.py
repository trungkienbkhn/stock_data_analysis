"""
WSGI config for myapp project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/3.1/howto/deployment/wsgi/
"""

import os
import time
from threading import Thread
from api.command.update_analysis_data import update_analysis_data
from django.core.wsgi import get_wsgi_application

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'myapp.settings')

application = get_wsgi_application()


def thread_to_update_data():
    while True:
        time.sleep(604800)
        update_analysis_data()


Thread(target=thread_to_update_data).start()
