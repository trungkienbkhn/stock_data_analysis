from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.http import HttpResponseRedirect 
from django.contrib.auth.models import User
from .forms import RegistrationForm, UpdateForm
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth import authenticate, login, logout
from django.contrib import messages 
import json
from jose import jwt
import datetime

def index(request):
    response = {
        'message': 'Welcome to my page' 
    }  
    return HttpResponse(json.dumps(response), content_type='application/json')

def show(request):
    if request.method == "GET":
        token = request.headers.get('Authorization')
        try:
            data = get_data_from_token(token)
            if data.get('is_admin'):
                users = User.objects.all()
                response = []
                for user in users:
                    if user.is_superuser:
                        role = 'Administrator'
                    else:
                        role = 'User'
                    user_form = {'id': user.id, 'username': user.username, 'email': user.email, 'role': role}
                    response.append(user_form)
            else:
                response = {
                    'status': False,
                    'error': 'Permission error'
                }
        except:
            response = {
                'status': False,
                'error': 'Authentication failed'
            }
        return HttpResponse(json.dumps(response), content_type='application/json')

@csrf_exempt
def login_view(request):
    if request.method == 'POST':
        request_body = json.loads(str(request.body, 'utf-8'))
        username = request_body.get('username')
        password = request_body.get('password')
        user = authenticate(request, username=username, password=password)
        if user is not None:
            login(request, user)
            payload = {
                'status': True,
                'id': user.id,
                'username': user.username,
                'email': user.email,
                'is_admin': user.is_superuser,
            }
            response = set_token(payload)
        else:
            response = {
                'status': False,
                'error': 'Username or password is invalid'
            }
        return HttpResponse(json.dumps(response), content_type='application/json')
    
@csrf_exempt
def logout_view(request):
    if request.user.is_authenticated:
        try:
            logout(request)
            response = {
                'status': True
            }
        except:
            response = {
                'status': False
            }
    else:
        response = {
            'status': False
        }
    return HttpResponse(json.dumps(response), content_type='application/json')

@csrf_exempt
def register(request):
    form = RegistrationForm()
    if request.method == 'POST':
        token = request.headers.get('Authorization')
        if token != 'null':
            response = {
                'status': False
            }
        else:
            form = RegistrationForm(json.loads(str(request.body, 'utf-8')))
            if form.is_valid():
                try:
                    form.save()
                    response = {
                        'status': True,
                    } 
                except ValueError as error:
                    response = {
                        'status': False,
                        'error': error
                    }  
            else:
                response = {
                    'status': False,
                    'error': form.errors
                }  
        return HttpResponse(json.dumps(response), content_type='application/json')

def edit(request, id):
    if request.method == 'GET':
        token = request.headers.get('Authorization')
        try:
            data = get_data_from_token(token)
            if (data.get('id') == id) or data.get('is_admin'):
                user = User.objects.filter(id=id)
                if len(user) == 0:
                    response = {
                        'status': False,
                        'error': 'User not found'
                    }
                else:
                    response = {
                        'status': True,
                        'id': user[0].id,
                        'username': user[0].username,
                        'email': user[0].email
                    }  
        except:
            response = {
                'status': False,
                'error': 'Authentication failed'
            }
        return HttpResponse(json.dumps(response), content_type='application/json')

@csrf_exempt
def update(request, id): 
    if request.method == 'POST':
        token = request.headers.get('Authorization')
        try:
            data = get_data_from_token(token)
            if (data.get('id') == id) or data.get('is_admin'):
                new_data = json.loads(str(request.body, 'utf-8'))
                user = User.objects.filter(id=id)
                user.update(username=new_data['username'], email=new_data['email'])
                response = {
                    'status': True,
                } 
            else:
                response = {
                    'status': False,
                    'error': "Permission error"
                }
        except:
            response = {
                'status': False,
                'error': "Authentication failed"
            }
        return HttpResponse(json.dumps(response), content_type='application/json')

@csrf_exempt
def reset_password(request): 
    if request.method == 'POST':
        token = request.headers.get('Authorization')
        try:
            data = get_data_from_token(token)
            new_data = json.loads(str(request.body, 'utf-8'))
            user = User.objects.get(id=data.get('id'))
            user.set_password(new_data['password'])
            user.save()
            response = {
                'status': True,
            }
        except:
            response = {
                'status': False,
                'error': "Authentication failed"
            }
        return HttpResponse(json.dumps(response), content_type='application/json')

@csrf_exempt
def destroy(request, id):
    if request.method == 'DELETE':
        token = request.headers.get('Authorization')
        try:
            data = get_data_from_token(token)
            if data.get('is_admin'):
                user = User.objects.filter(id=id)
                if len(user) != 0:
                    user[0].delete()
                    response = {
                        'status': True
                    }
                else:
                    response = {
                        'status': False,
                        'error': 'Not found user'
                    }
        except:
            response = {
                'status': False,
                'error': 'Authentication failed'
            }
        return HttpResponse(json.dumps(response), content_type='application/json')   

def profile(request):
    if request.method == "GET":
        token = request.headers.get('Authorization')
        try:
            data = get_data_from_token(token)
            user = User.objects.get(id = data.get('id'))
            response = {'id': user.id, 'username': user.username, 'email': user.email}
        except:
            response = {
                'status': False,
                'error': 'Authentication failed'
            }
        return HttpResponse(json.dumps(response), content_type='application/json')

def set_token(payload):
    expired_time = datetime.datetime.utcnow() + datetime.timedelta(days=1)
    payload['exp'] = expired_time
    token = jwt.encode(payload, 'stock_analysis', algorithm='HS256')
    payload['token'] = token
    return payload

def get_data_from_token(token):
    data = jwt.decode(token.replace('Bearer ',''), 'stock_analysis', algorithms=['HS256'])
    return data
