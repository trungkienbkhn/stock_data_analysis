from django.urls import path
from . import views
from django.contrib.auth import views as auth_views

urlpatterns = [
    path('', views.index),
    path('users',views.show, name="users"),  
    path('register', views.register, name="register"),
    path('login',views.login_view, name="login"),
    path('logout',views.logout_view, name='logout'),
    path('user/edit/<int:id>', views.edit, name="edit"),  
    path('user/update/<int:id>', views.update, name="update"),
    path('user/reset_pwd', views.reset_password, name="reset_password"),  
    path('user/delete/<int:id>', views.destroy, name="destroy"),
    path('user/profile', views.profile, name="user")
]
