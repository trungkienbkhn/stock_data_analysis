import requests
import json
import pandas as pd
import plotly.graph_objects as go
from datetime import datetime

def get_url(url, headers, data=''):
    response = requests.get(url, headers=headers, data=json.dumps(data))
    return json.loads(response.text)

def get_price(code):
    url = 'https://finance.vietstock.vn/company/tradinginfo'
    headers = {
        "Content-Type": "application/json; charset=utf-8",
        "User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.182 Safari/537.36",
        "Host": "finance.vietstock.vn",
        "Origin": "https://finance.vietstock.vn"
    }
    data = {"code": code, 's': 0}
    return get_url(url, headers, data)


def get_index():
    url = 'https://finance.vietstock.vn/data/getmarketprice?type=2'
    headers = {
        "Content-Type": "application/json; charset=utf-8",
        "User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.182 Safari/537.36",
        "Host": "finance.vietstock.vn",
        "Origin": "https://finance.vietstock.vn"
    }
    return get_url(url, headers)


def draw_chart(stock):
    date = []
    high = []
    low = []
    open = []
    close = []
    totalVal = []
    totalVol = []
    for i in range(len(stock)):
        timestamp = stock[i]['TradingDate'].replace('/Date(', '').replace(')/', '')
        time = datetime.fromtimestamp(int(timestamp) / 1000)
        date.append(time)
        high.append(stock[i]['HighestPrice'])
        low.append(stock[i]['LowestPrice'])
        open.append(stock[i]['OpenPrice'])
        close.append(stock[i]['ClosePrice'])
        totalVal.append(stock[i]['TotalVal'])
        totalVol.append(stock[i]['TotalVol'])

    d = {
        'date': date,
        'high': high,
        'low': low,
        'open': open,
        'close': close,
        'total_val': totalVal,
        'total_vol': totalVol
    }
    df = pd.DataFrame(data=d)
    df = df.iloc[::-1]

    fig = go.Figure(data=[go.Candlestick(x=df['date'],
                                         open=df['open'],
                                         high=df['high'],
                                         low=df['low'],
                                         close=df['close'])])
    fig.update_layout(xaxis_rangeslider_visible=False)
    fig.show()
