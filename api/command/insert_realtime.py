from datetime import datetime, timezone, timedelta

from influxdb import InfluxDBClient

def get_client(host, port, db_name):
    client = InfluxDBClient(host=host, port=port)
    client.switch_database(db_name)
    return client


def insert_index(db, data):
    json_data = [
        {
            'measurement': 'index',
            'tags': {
                'code': data['name']
            },
            'fields': {
                'datetime': data['datetime'],
                'index': data['index'],
                'change': data['change'],
                'percent': data['percent'],
                'volume': data['volume'],
                'value': data['value']
            }
        }
    ]
    db.write_points(json_data)


def insert_stock(db, data):
    json_data = [
        {
            'measurement': 'stock_' + str(data['code']),
            'tags': {
                'code': data['code']
            },
            'fields': {
                'datetime': data['datetime'],
                'open': data['open'],
                'close': data['close'],
                'highest': data['highest'],
                'lowest': data['lowest']
            }
        }
    ]
    db.write_points(json_data)

def query_data(query_str, code, db):
    client = InfluxDBClient(host='localhost', port='8086')
    client.switch_database(db)
    query = client.query(query_str, epoch='s')
    if code != 'null':
        return list(query.get_points(tags={'code': code}))
    else:
        return list(query.get_points())


def convert_index(query_result):
    time = []
    code = []
    date = []
    index = []
    change = []
    percent = []
    value = []
    volume = []
    for index_code in query_result:
        if float(index_code['index'].replace(',', '')) > 0:
            time.append(datetime.fromtimestamp(index_code['time']).astimezone(timezone(timedelta(hours=7))).strftime("%Y-%m-%d %H:%M:%S"))
            code.append(index_code['code'])
            date.append(index_code['date'])
            index.append(index_code['index'])
            change.append(index_code['change'])
            percent.append(index_code['percent'])
            value.append(index_code['value'])
            volume.append(index_code['volume'])

    json_array = {
        'time': time,
        'code': code,
        'date': date,
        'index': index,
        'change': change,
        'percent': percent,
        'value': value,
        'volume': volume
    }
    return json_array


def response_api(query_str, code='null', type='array', db='stockdb'):
    query_result = query_data(query_str, code, db)
    if type == 'array':
        return query_result
    elif type == 'index':
        return convert_index(query_result)
    else:
        return []
