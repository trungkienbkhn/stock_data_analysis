import requests
from scrapy.selector import Selector
import pandas as pd
import psycopg2
from datetime import datetime
from command.data_tools.preprocess_data import preprocess_data, download_pdf
from command.data_tools.extract_data import extract_data
from command.psql_tools.get_data_from_db import get_number_of_ids, get_list_stock_code
from stock.models import Stock, Bookmark
from django.contrib.auth.models import User
from command.email.email_utils import send_notification_email
from threading import Thread

def update_analysis_data():
    # get date check
    conn = psycopg2.connect(database="stock",
                            user="postgres",
                            password="123",
                            host="localhost",
                            port="5432")

    cur = conn.cursor()
    cmd = """select distinct crawled_time
            from stock_analysis 
            order by crawled_time DESC"""
    cur.execute(cmd)
    cur_result = str(cur.fetchall()[0]).split('date(')[1].split(')')[0].replace(',', '-').replace(' ', '')
    date_check = datetime.strptime(cur_result.strip(), "%Y-%m-%d")

    # get data raw
    url = "https://www.stockbiz.vn/AnalysisReports.aspx?Type=1"
    reports = []
    dates = []
    links = []
    sources = []
    reports_name = []
    break_check = 0
    j = 0

    while (break_check == 0):
        j += 1
        payload={'Cart_ctl00_webPartManager_wp372089597_wp604056795_callbackReports_Callback_Param': str(j)}
        response = requests.request("POST", url, data=payload)
        body = response.text
        date = Selector(text=body).xpath('//td[@valign="top"]/text()').getall()
        k = 3
        for i in range(1,21):
            d = datetime.strptime(date[k].strip(),"%d/%m/%Y")
            if (d > date_check):
                dates.append(d.strftime('%Y-%m-%d'))
                k+=11
                if i < 10 :
                    params = '//a[@id="ctl00_webPartManager_wp372089597_wp604056795_rptReports_ctl'+'0'+str(i)+'_lnkTitle"]/@href'
                    params_name = '//a[@id="ctl00_webPartManager_wp372089597_wp604056795_rptReports_ctl'+'0'+str(i)+'_lnkTitle"]/text()'
                    src_params = '//a[@id="ctl00_webPartManager_wp372089597_wp604056795_rptReports_ctl'+'0'+str(i)+'_lnkSource"]/text()'
                else:
                    params = '//a[@id="ctl00_webPartManager_wp372089597_wp604056795_rptReports_ctl'+str(i)+'_lnkTitle"]/@href'
                    params_name = '//a[@id="ctl00_webPartManager_wp372089597_wp604056795_rptReports_ctl'+str(i)+'_lnkTitle"]/text()'
                    src_params = '//a[@id="ctl00_webPartManager_wp372089597_wp604056795_rptReports_ctl'+str(i)+'_lnkSource"]/text()'
                report = Selector(text=body).xpath(params).get()
                source = Selector(text=body).xpath(src_params).get()
                report_name = Selector(text=body).xpath(params_name).get()
                reports.append(report)
                sources.append(source)
                id = report.split("/")[4]
                link = 'https://www.stockbiz.vn/Handlers/DownloadReport.ashx?ReportID='+str(id)
                links.append(link)
                reports_name.append(report_name)
            else:
                break_check = 1
                break

    data_raw = pd.DataFrame()
    data_raw['titles'] = reports_name
    data_raw['links_report'] = reports
    data_raw['links_download'] = links
    data_raw['sources'] = sources
    data_raw['published_dates'] = dates

    #preprocess data
    data_preprocess = preprocess_data(data_raw)
    if data_preprocess.empty:
        msg = "No data update"
        return msg
    else:
        download_pdf(data_preprocess)
        data_extract = extract_data(data_preprocess)
        if data_extract.empty:
            msg = "No data update"
            return msg
        else:
            # insert data to database
            number_ids = get_number_of_ids(cur)
            for i in range(0, len(data_extract)):
                id = number_ids + i + 1
                stock_id = Stock.objects.get(code=data_extract.iloc[i].stock_code.upper()).id
                bookmarks = Bookmark.objects.filter(stock_id=stock_id)
                if len(bookmarks) > 0:
                    send_notification(bookmarks)
                cur.execute('''insert into stock_analysis(id, stock_id, target_price, current_price, rate, recommendation, title, sector, source,
                                                    link_download, link_report, published_date, crawled_time)
                                values({}, {}, {}, {}, '{}', '{}', '{}', '{}', '{}', '{}', '{}', '{}', '{}')'''.format(id, stock_id,
                                data_extract.iloc[i].target_price, data_extract.iloc[i].current_price, data_extract.iloc[i].rate,
                                data_extract.iloc[i].recommendation, data_extract.iloc[i].titles, data_extract.iloc[i].sector,
                                data_extract.iloc[i].sources, data_extract.iloc[i].links_download, data_extract.iloc[i].links_report,
                                data_extract.iloc[i].published_dates, data_extract.iloc[i].crawled_time))

            conn.commit()
            conn.close()
            msg = "ok"
            return msg

def send_notification(bookmarks):
    for bookmark in bookmarks:
        user_infos = User.objects.get(id=bookmark.user_id)
        stock_infos = Stock.objects.get(id=bookmark.stock_id)
        Thread(target=send_notification_email, args=(user_infos, stock_infos)).start()
