import psycopg2
import sys, os
import pandas as pd
conn = psycopg2.connect(database="stock",
                        user="postgres",
                        password="123",
                        host="localhost",
                        port="5432")

cur = conn.cursor()
cmd = "DELETE FROM stock_analysis"
cur.execute(cmd)
conn.commit()
print("Operation done successfully")
conn.close()
