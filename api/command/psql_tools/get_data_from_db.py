import psycopg2
import sys, os
import pandas as pd

def get_number_of_ids(cur):
    cmd = "select max(id) from stock_analysis"
    cur.execute(cmd)
    results = cur.fetchall()[0]
    return int(list(results)[0])

def get_list_stock_code(cur):
    cmd = "select code from stock_stock order by id"
    cur.execute(cmd)
    results = cur.fetchall()
    stocks = []
    for res in results:
        stocks.append(str(list(res)[0]).upper())
    return stocks
