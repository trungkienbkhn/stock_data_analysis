import psycopg2
import sys, os
import pandas as pd
conn = psycopg2.connect(database="stock",
                        user="postgres",
                        password="123",
                        host="localhost",
                        port="5432")

cur = conn.cursor()
cmd = """CREATE OR REPLACE FUNCTION process_notif() RETURNS TRIGGER AS $notif$ 
            BEGIN
                IF (TG_OP = 'INSERT') AND ((select id from stock_bookmark where stock_id = New.stock_id) IS NOT NULL) THEN
                    set timezone='Asia/Ho_Chi_Minh';
                    insert into stock_notification(analysis_id, bookmark_id, created_time)
                    values (New.id, (select id from stock_bookmark where stock_id = New.stock_id), current_timestamp);
                END IF;
                RETURN NULL;
            END;
        $notif$ LANGUAGE plpgsql;

        CREATE TRIGGER notif
        AFTER INSERT ON stock_analysis
        FOR EACH ROW EXECUTE PROCEDURE process_notif();"""

cur.execute(cmd)
conn.commit()
print("Operation done successfully")
conn.close()
