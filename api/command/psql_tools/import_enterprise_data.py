import psycopg2
import sys, os
import pandas as pd
conn = psycopg2.connect(database="stock",
                        user="postgres",
                        password="123",
                        host="localhost",
                        port="5432")

cur = conn.cursor()
path_to_data = os.getcwd()+'/command/data/enterprise_details.csv'
cmd = "COPY stock_stock FROM "+"'"+path_to_data+"'"+" DELIMITER ',' CSV HEADER;"
cur.execute(cmd)
conn.commit()
print("Operation done successfully")
conn.close()
