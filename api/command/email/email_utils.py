import json
import os
import smtplib
from string import Template
from email.mime.text import MIMEText

MAIL_SERVER = {
    "gmail": ("smtp.gmail.com", 587),
    "office365": ("smtp.office365.com", 587)
}


class EmailSender:
    def __init__(self, username, password, email_type, display_name):
        self.username = username
        self.password = password
        self.email_type = email_type
        self.display_name = display_name
        self.server = None

    def login(self):
        server_address = MAIL_SERVER.get(self.email_type)
        if not server_address:
            raise Exception(f"Invalid type: {self.email_type}")
        server = smtplib.SMTP(*server_address)
        server.starttls()
        server.login(self.username, self.password)
        self.server = server

    def quit(self):
        self.server.quit()

    def send_mail(self, receivers, email):
        email_string = email.as_string()
        self.server.sendmail(self.username, receivers, email_string)


class Email:
    def __init__(self, subject, body, sender_display_name, receivers):
        message = MIMEText(body, "html")
        message['Subject'] = subject
        message['From'] = sender_display_name
        message['To'] = ", ".join(receivers)

        self.message = message

    def as_string(self):
        return self.message.as_string()


class EmailUtils:
    @staticmethod
    def send_text_mail(subject, body, receivers, sender=None, logout_after_send=True):
        if not sender:
            logout_after_send = True
            sender = EmailUtils.get_sender()

        sender.login()
        email = Email(subject, body, sender.display_name, receivers)
        sender.send_mail(receivers, email)
        if logout_after_send:
            sender.quit()

    @classmethod
    def get_email_config(cls):
        f = open(os.path.dirname(os.path.realpath(__file__)) + "/email_config.json", 'r')
        email_config = json.loads(f.read())
        return email_config

    @classmethod
    def get_sender(cls):
        email_config = cls.get_email_config()
        sender = EmailSender(email_config["username"], email_config["password"], email_config["type"],
                             email_config.get("display_name"))
        return sender

    @classmethod
    def get_email_body_template(cls):
        f = open(os.path.dirname(os.path.realpath(__file__)) + "/email_template.html", 'r')
        return f.read()


def send_notification_email(user_infos, stock_infos):
    email_config = EmailUtils.get_email_config()
    username = user_infos.username
    user_email = user_infos.email
    stock_code = stock_infos.code
    link = email_config['link'] + str(stock_infos.id)
    email_subject = "Stock analysis notification"
    body_template = EmailUtils.get_email_body_template()
    email_body = Template(body_template).safe_substitute(username=username, stock_code=stock_code, link=link)

    EmailUtils.send_text_mail(email_subject, email_body, [user_email])
